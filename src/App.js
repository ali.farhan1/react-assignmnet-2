import React, { useEffect, useState, useCallback } from "react";
import axios from "axios";
import { Row } from "antd";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";
import Modal from "./components/Modal";
import Progress from "./components/Progress";
import UserCard from "./components/UserCard";
import "./App.css";

const App = () => {
    const [userData, setUserData] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const [isModelOpen, setModelOpen] = React.useState(false);
    const [currentUser, setCurrentUser] = React.useState({});

    const toggleModal = () => {
        setModelOpen((prev) => !prev);
    };

    const deleteUser = (id) => {
        setUserData((prevState) => {
            const users = prevState.filter((user) => user.id !== id);
            console.log("USERS", users);
            return [...users];
        });
    };

    const updateUser = (id, data) => {
        setUserData((prevState) => {
            const index = prevState.findIndex((user) => user.id === id);
            const users = [...prevState];
            users[index] = { ...users[index], ...data };
            return [...users];
        });
    };

    const likeUser = (id) => {
        setUserData((prevState) => {
            const index = prevState.findIndex((user) => user.id === id);
            const users = [...prevState];
            users[index] = { ...users[index], like: !users[index].like };
            return [...users];
        });
    };

    const handleModal = useCallback(
        (id) => {
            setCurrentUser(userData.find((user) => user.id === id));
        },
        [userData]
    );

    useEffect(() => {
        setLoading(true);
        axios
            .get("https://jsonplaceholder.typicode.com/users")
            .then(function (response) {
                console.log(response);
                const users = response.data.map((user) => {
                    return {
                        ...user,
                        like: false,
                    };
                });
                console.log("USERS", users);
                setUserData(users);
                setLoading(false);
            })
            .catch(function (error) {
                // handle error
                console.log(error, "state");
            });
    }, []);

    return isLoading ? (
        <Progress />
    ) : (
        <div className="App">
            <NavBar />
            <Row gutter={[10, 24]} className="user-cards">
                <Modal
                    toggleModal={toggleModal}
                    isOpen={isModelOpen}
                    user={currentUser}
                    onEdit={updateUser}
                />
                {userData.length > 0 &&
                    userData.map((user) => (
                        <UserCard
                            user={user}
                            likeUser={likeUser}
                            handleModal={handleModal}
                            deleteAction={deleteUser}
                            toggleModal={toggleModal}
                        />
                    ))}
            </Row>
            <Footer />
        </div>
    );
};

export default App;
