// page header with add new button with plus icon

import React from "react";
import { PageHeader } from "antd";

const NavBar = () => {
    return (
        <div className="navbar">
            <PageHeader
                className="site-page-header"
                title="Assignment 2"
                subTitle="Farhan Kiyani"
            />
        </div>
    );
};

export default NavBar;
