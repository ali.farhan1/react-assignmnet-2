import React from "react";
import propTypes from "prop-types";
import defaultProps from "default-props";
import { Card, Col } from "antd";
import {
    HeartOutlined,
    EditOutlined,
    DeleteOutlined,
    MailOutlined,
    PhoneOutlined,
    GlobalOutlined,
    HeartFilled,
} from "@ant-design/icons";

const { Meta } = Card;

const UserCard = ({
    user,
    deleteAction,
    handleModal,
    toggleModal,
    likeUser,
}) => {
    const handleLike = () => {
        likeUser(user.id);
    };

    const handleEdit = () => {
        toggleModal();
        handleModal(user.id);
    };

    const handleDelete = () => {
        deleteAction(user.id);
    };

    return (
        <React.Fragment key={user.username}>
            <Col xs={24} sm={24} md={8} lg={8} xl={6}>
                <Card
                    className="user--card"
                    cover={
                        <img
                            alt={user.username}
                            src={`https://avatars.dicebear.com/v2/avataaars/${user.username}.svg?options[mood][]=happy`}
                            width={200}
                        />
                    }
                    actions={[
                        <button onClick={handleLike} className="card-action">
                            {user.like ? (
                                <HeartFilled className="card-action-like card-action-icon" />
                            ) : (
                                <HeartOutlined className="card-action-dislike card-action-icon" />
                            )}
                        </button>,
                        <button onClick={handleEdit} className="card-action">
                            <EditOutlined
                                key="edit"
                                className="card-action-icon"
                            />
                        </button>,
                        <button onClick={handleDelete} className="card-action">
                            <DeleteOutlined
                                key="delete"
                                className="card-action-icon"
                            />
                        </button>,
                    ]}
                >
                    <Meta title={user.name} />
                    <div className="user--card__info">
                        <p>
                            <MailOutlined /> {user.email} <br />
                            <PhoneOutlined /> {user.phone} <br />
                            <GlobalOutlined /> {user.website}
                        </p>
                    </div>
                </Card>
            </Col>
        </React.Fragment>
    );
};

UserCard.propTypes = {
    user: propTypes.object.isRequired,
    handleDelete: propTypes.func.isRequired,
    handleModal: propTypes.func.isRequired,
};

UserCard.defaultProps = defaultProps;

UserCard.defaultProps = {
    user: {},
    deleteAction: () => {},
    handleModal: () => {},
    toggleModal: () => {},
    likeUser: () => {},
};

export default UserCard;
