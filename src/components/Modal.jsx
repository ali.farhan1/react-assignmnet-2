// An antd modal that contained a form is populated with user data

import React, { useEffect } from "react";
import { Modal, Form, Input } from "antd";

const Model = ({ user, onEdit, isOpen, toggleModal }) => {
    const [form] = Form.useForm();

    const handleSubmit = () => {
        form.validateFields()
            .then((values) => {
                onEdit(user.id, values);
                toggleModal();
            })
            .catch((info) => {
                console.log("Validate Failed:", info);
            });
    };

    useEffect(() => {
        form.resetFields();
    }, [isOpen, form]);

    return (
        <React.Fragment key={user.username}>
            <Modal
                visible={isOpen}
                title="Edit User"
                okText="Submit"
                cancelText="Cancel"
                onCancel={toggleModal}
                onOk={handleSubmit}
            >
                <Form
                    form={form}
                    layout="vertical"
                    name="update_user"
                    initialValues={{
                        modifier: "public",
                    }}
                >
                    <Form.Item
                        name="name"
                        label="Name"
                        initialValue={user.name}
                        rules={[
                            {
                                required: true,
                                message: "This field is required",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="Email"
                        initialValue={user.email}
                        rules={[
                            {
                                required: true,
                                message: "This field is required",
                            },
                            {
                                type: "email",
                                message: "Invalid email",
                            },
                        ]}
                    >
                        <Input type="email" />
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        label="Phone"
                        initialValue={user.phone}
                        rules={[
                            {
                                required: true,
                                message: "This field is required",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="website"
                        label="Website"
                        initialValue={user.website}
                        rules={[
                            {
                                required: true,
                                message: "This field is required",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                </Form>
            </Modal>
        </React.Fragment>
    );
};

export default Model;
