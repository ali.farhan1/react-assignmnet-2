// Import Statements

import React from "react";
import { Typography } from "antd";

const { Text, Link } = Typography;

// Definition Statements

const Footer = () => (
    <div className={`footer`}>
        <Text variant="body2">
            {"Copyright © "}
            <Link
                color="inherit"
                href="https://www.linkedin.com/in/farhan-kiyani/"
            >
                Farhan Kiyani
            </Link>{" "}
            {new Date().getFullYear()}
            {"."}
        </Text>
    </div>
);

export default Footer;
